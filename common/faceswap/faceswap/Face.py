import cv2
import dlib
import numpy
import random
import numpy as np
from utils import *

SCALE_FACTOR = 1
FEATHER_AMOUNT = 11

FACE_POINTS = list(range(17, 68))
MOUTH_POINTS = list(range(48, 61))
RIGHT_BROW_POINTS = list(range(17, 22))
LEFT_BROW_POINTS = list(range(22, 27))
RIGHT_EYE_POINTS = list(range(36, 42))
LEFT_EYE_POINTS = list(range(42, 48))
NOSE_POINTS = list(range(27, 35))
JAW_POINTS = list(range(0, 17))



# Amount of blur to use during colour correction, as a fraction of the
# pupillary distance.
# COLOUR_CORRECT_BLUR_FRAC = 0.6

PREDICTOR_PATH = "faceswap/shape_predictor_68_face_landmarks.dat"

class Face:

    def __init__(self, fname, image):

        self.detector = dlib.get_frontal_face_detector()
        self.predictor = dlib.shape_predictor(PREDICTOR_PATH)
        self.read_im_and_landmarks(fname, image)
        self.cropped = False
        self.cropped_face = None

    def rect_to_bb(self, rect):
        # take a bounding predicted by dlib and convert it
        # to the format (x, y, w, h) as we would normally do
        # with OpenCV
        x = rect.left()
        y = rect.top()
        w = rect.right() - x
        h = rect.bottom() - y

        # return a tuple of (x, y, w, h)
        return x, y, w, h

    def get_landmarks(self, im):
        rects = self.detector(im, 1)

        if len(rects) == 0:
            return 0, len(rects)

        # for many faces
        k = random.randrange(0, len(rects))

        return numpy.matrix([[p.x, p.y] for p in self.predictor(im, rects[k]).parts()]), len(rects)

    # 이미지 landmark 추출
    def read_im_and_landmarks(self, fname, image):
        if fname !=None:
            self.image= cv2.imread(fname, cv2.IMREAD_COLOR)
        else:
            self.image = image

        global SCALE_FACTOR
        SCALE_FACTOR = self.image.shape[1] / 1000

        if SCALE_FACTOR < 1: SCALE_FACTOR = 1
        resized = cv2.resize(self.image, (int(self.image.shape[1] / SCALE_FACTOR),
                             int(self.image.shape[0] / SCALE_FACTOR)))
        s, valid = self.get_landmarks(resized)
        s = np.array(s)
        for mark in s:
            mark[0] = int(mark[0] * SCALE_FACTOR)
            mark[1] = int(mark[1] * SCALE_FACTOR)
        self.landmarks = s

    # 두 landmark 간 각도
    def arctan_pts(self, points):
        return np.arctan(
            (self.landmarks[points[0], 1] - self.landmarks[points[1], 1]) / (self.landmarks[points[0], 0] - self.landmarks[points[1], 0]))

    # 두 landmark 간 거리
    def distance_pts(self, points):
        return np.power(np.power((self.landmarks[points[0], 1] - self.landmarks[points[1], 1]), 2) + np.power(
            (self.landmarks[points[0], 0] - self.landmarks[points[1], 0]), 2), 0.5)

    # 두 landmark 간 중간값
    def mid_pt(self, points):
        p1 = self.landmarks[points[0]]
        p2 = self.landmarks[points[1]]
        return np.divide(np.add(p1, p2), 2)

    # landmark의 2번째 점에 대한 1번 점의 대칭점
    def out_pt(self, points):
        p1 = self.landmarks[points[0]]
        p2 = self.landmarks[points[1]]
        return np.add(np.subtract(p2, p1), p2)

    # l1 l2 사리 거리 * amount 한 만큼 l2 밖에 있는 점 계산
    def out_pt_amount(self, l1, l2, amount):
        p1 = self.landmarks[l1]
        p2 = self.landmarks[l2]
        return np.add(np.subtract(p2, p1)* (1+amount), p2)

    # l1 l2 사리 거리 * amount 한 만큼 l1, l2 사이에 있는 점 계산
    def in_pt_amount(self, l1, l2, amount):
        p1 = self.landmarks[l1]
        p2 = self.landmarks[l2]
        return np.add(np.subtract(p2, p1)* amount, p1)

    # 2*3 Affine Matrix에 대하여 image affine transform 후 landmark도 함께 변경
    def warpAffine(self, M, Size):
        self.image = cv2.warpAffine(self.image, M, Size)
        self.read_im_and_landmarks(None,self.image)


    def facecrop(self, size):
        self.read_im_and_landmarks(None, self.image)
        height, width = self.image.shape[:2]
        dets, scores, idx = self.detector.run(self.image, 1, -1)
        faces = list(map(list, zip(dets, scores)))
        list.sort(faces, key=lambda x: x[1], reverse=True)

        det = faces[0][0]
        shape = self.predictor(self.image, det)
        left_eye = extract_left_eye_center(shape)
        right_eye = extract_right_eye_center(shape)
        M = get_rotation_matrix(left_eye, right_eye)
        rotated = cv2.warpAffine(self.image, M, (width, height), flags=cv2.INTER_CUBIC)

        dets, scores, idx = self.detector.run(rotated, 1, -1)
        faces = list(map(list, zip(dets, scores)))
        list.sort(faces, key=lambda x: x[1], reverse=True)
        det = faces[0][0]

        x, y, w, h = self.rect_to_bb(det)
        center = x + w / 2, y + h / 2
        x = int(center[0] - w / 2 * 1.6)
        y = int(center[1] - h / 2 * 2.2)
        shape = self.predictor(rotated, det)
        left_eye = extract_left_eye_center(shape)
        right_eye = extract_right_eye_center(shape)
        eye_center = (int((left_eye[0] + right_eye[0]) / 2), int((left_eye[1] + right_eye[1]) / 2))
        m = max(eye_center[0] - x, eye_center[1] - y)

        w, h, _ = np.shape(rotated)

        x1, x2, y1, y2 =  eye_center[0] - m , eye_center[0] + m, eye_center[1] - m, eye_center[1] + m
        if x1 < 0:
            x2 -= x1
            x1 = 0
        if x2 > w-1:
            x1 -= (x2 - w)
            x2 = w-1

        if y1 < 0:
            y2 -= y1
            y1 =0
        if y2 > h-1:
            y1 -= (y2-h)
            y2 = h-1


        rotated = rotated[y1:y2, x1:x2, :]
        rotated = cv2.resize(rotated, (size, size), interpolation=cv2.INTER_LINEAR)
        if random.random() < 0.5:
            rotated = cv2.flip(rotated, 1)

        self.cropped_face = rotated
        self.cropped = True
        return True
