from blend_modes import blend_modes
import cv2
import numpy as np

def blending(source, target, mask, blending_type, opacity):
    original = np.copy(target)
    height, width = target.shape[:2]
    target = target.astype('uint8')
    source = source.astype('uint8')
    source = cv2.resize(source, (width, height))
    target = cv2.cvtColor(target, cv2.COLOR_RGB2RGBA)
    source = cv2.cvtColor(source, cv2.COLOR_RGB2RGBA)
    target = target.astype('float')
    source = source.astype('float')

    target_copy = np.copy(target)
    source_copy = np.copy(source)
    if blending_type=='soft_light' or blending_type==1:
        blanding_mask = blend_modes.soft_light(target_copy, source_copy, opacity)
    elif blending_type=='lighten_only' or blending_type==2:
        blanding_mask = blend_modes.lighten_only(target_copy, source_copy, opacity)
    elif blending_type=='dodge' or blending_type==3:
        blanding_mask = blend_modes.dodge(target_copy, source_copy, opacity)
    elif blending_type=='addition' or blending_type==4:
        blanding_mask = blend_modes.addition(target_copy, source_copy, opacity)
    elif blending_type=='darken_only' or blending_type==5:
        blanding_mask = blend_modes.darken_only(target_copy, source_copy, opacity)
    elif blending_type=='multiply' or blending_type==6:
        blanding_mask = blend_modes.multiply(target_copy, source_copy, opacity)
    elif blending_type=='hard_light' or blending_type==7:
        blanding_mask = blend_modes.hard_light(target_copy, source_copy, opacity)
    elif blending_type=='difference' or blending_type==8:
        blanding_mask = blend_modes.difference(target_copy, source_copy, opacity)
    elif blending_type=='subtract' or blending_type==9:
        blanding_mask = blend_modes.subtract(target_copy, source_copy, opacity)
    elif blending_type=='grain_extract' or blending_type==10:
        blanding_mask = blend_modes.grain_extract(target_copy, source_copy, opacity)
    elif blending_type=='grain_merge' or blending_type==11:
        blanding_mask = blend_modes.grain_merge(target_copy, source_copy, opacity)
    elif blending_type=='divide' or blending_type==12:
        blanding_mask = blend_modes.divide(target_copy, source_copy, opacity)
    elif blending_type=='overlay' or blending_type==13:
        blanding_mask = blend_modes.overlay(target_copy, source_copy, opacity)
    elif blending_type=='normal' or blending_type==14:
        blanding_mask = blend_modes.normal(target_copy, source_copy, opacity)

    blanding_mask = blanding_mask.astype('uint8')
    blanding_mask = cv2.cvtColor(blanding_mask, cv2.COLOR_RGBA2RGB)
    output_im = original
    output_im = mask*blanding_mask + (1-mask)*original

    return output_im