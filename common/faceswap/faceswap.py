import cv2
import glob
from faceswap.Face import Face
import random
import datetime
import numpy as np
import os
from blending import blending


from joblib import Parallel, delayed

now = datetime.datetime.now()


class TooManyFaces(Exception):
    pass


class NoFaces(Exception):
    pass


def rotate2d(point, angle):
    out = [0, 0]
    out[1] = np.cos(angle) * point[1] - np.sin(angle) * point[0];
    out[0] = np.sin(angle) * point[1] - np.cos(angle) * point[0];

    return out


def rotatePoint(point, center, angle):
    return np.add(rotate2d(np.subtract(point, center), angle), center)




leftchin = list(range(0, 4))
rightchin = list(range(16, 12, -1))
leftcorner = [36, 48]
rightcorner = [54, 45]

jaw = list(range(4, 13))
leftjaw = list(range(4, 9))
rightjaw = list(range(8, 13))

uplips = list(range(48, 55))
leftuplips = list(range(48, 52))
rightuplips = list(range(51, 55))

lips = [48, 60, 59, 58, 57, 56, 55, 54, 64]
downlips = [48, 60, 59, 58, 57, 56, 55, 54, 64]
leftdownlips =list(range(57, 61))
rightdownlips = list(range(54, 58))

nosecolumn = list(range(28, 31))
nosefloor = list(range(31, 36))

leftbrow = list(range(19, 21))
rightbrow = list(range(23, 24))

lefteye = list(range(36, 39))
leftupeye = [37, 38]
leftdowneye = [41, 40]

righteye = list(range(42, 46))
rightupeye = [43, 44]
rightdowneye = [47, 46]

pairs = [[36, 45], [27, 57], [36, 27], [27, 45], [0,16]]

class FacePart:
    FACE = 0
    EYE = 1
    NOSE = 2
    MOUTH = 3
    EAR = 4
    NOSE_WITH_MOUTH = 5
    NOSE_WITH_EYE = 6

    Easy = True
    Hard = False

    @staticmethod
    def partPoints(part):
        if part == FacePart.FACE:
            midpoints = \
                [[random.choice(rightchin), random.choice(rightcorner), 0.5 * np.random.normal(1, 0.2)] for i in
                 range(30)] + \
                [[random.choice(leftchin), random.choice(leftcorner), 0.5 * np.random.normal(1, 0.2)] for i in
                 range(30)] + \
                [[random.choice(jaw), random.choice(lips), 0.5 * np.random.normal(1, 0.1)] for i in range(30)] + \
                [[random.choice([0, 1, 2]), random.choice([17, 18, 36]), 0.8 * np.random.normal(1, 0.1)] for i in
                 range(30)] + \
                [[random.choice([16, 15, 14]), random.choice([26, 45, 25]), 0.8 * np.random.normal(1, 0.1)] for i in
                 range(30)]

            outpoints = \
                [[random.choice(righteye), random.choice(rightbrow), 0.5 * np.random.normal(1, 0.3)] for i in
                 range(20)] + \
                [[random.choice(lefteye), random.choice(leftbrow), 0.5 * np.random.normal(1, 0.3)] for i in range(20)]

        elif part == FacePart.EYE:
            midpoints = \
                [[random.choice([0, 1, 2]), random.choice([17, 18, 36]), 0.5 * np.random.normal(1, 0.1)] for i in
                 range(30)] + \
                [[random.choice([16, 15, 14]), random.choice([26, 45, 25]), 0.5 * np.random.normal(1, 0.1)] for i in
                 range(30)]

            outpoints = \
                [[random.choice(rightupeye), random.choice(rightdowneye), 0.5 * np.random.normal(1, 0.1)] for i in
                 range(20)] + \
                [[random.choice(rightdowneye), random.choice(rightupeye), 0.8 * np.random.normal(1, 0.3)] for i in
                 range(20)] + \
                [[random.choice(leftupeye), random.choice(leftdowneye), 0.5 * np.random.normal(1, 0.1)] for i in
                 range(20)] + \
                [[random.choice(leftdowneye), random.choice(leftupeye), 0.8 * np.random.normal(1, 0.3)] for i in
                 range(20)]

        elif part == FacePart.NOSE:
            midpoints = \
                [[random.choice(rightdowneye), random.choice(nosecolumn), 0.5 * np.random.normal(1, 0.2)] for i in
                range(30)] + \
                [[random.choice(leftdowneye), random.choice(nosecolumn), 0.5 * np.random.normal(1, 0.2)] for i in
                range(30)] + \
                [[random.choice(nosefloor), random.choice(rightdowneye), 0.2 * np.random.normal(1, 0.5)] for i in
                range(30)] + \
                [[random.choice(nosefloor), random.choice(leftdowneye), 0.2 * np.random.normal(1, 0.5)] for i in
                range(30)] + \
                [[random.choice(nosefloor), random.choice(uplips), 0.5 * np.random.normal(1, 0.2)] for i in
                range(30)]

            outpoints = \
                [[random.choice(nosefloor), random.choice(nosefloor), 0.1 * np.random.normal(1,0.3)] for i in
                range(20)]

        elif part == FacePart.MOUTH:
            midpoints = \
                [[random.choice(uplips), random.choice(nosefloor), 0.5 * np.random.normal(1, 0.2)] for i in
                range(30)] + \
                [[random.choice(leftdownlips), random.choice(leftjaw), 0.2 * np.random.normal(1, 0.2)] for i in
                 range(30)] + \
                [[random.choice(rightdownlips), random.choice(rightjaw), 0.2 * np.random.normal(1, 0.2)] for i in
                 range(30)] + \
                [[random.choice([48]), random.choice([2, 3, 4, 5]), 0.2 * np.random.normal(1, 0.2)] for i in
                 range(30)] + \
                [[random.choice([54]), random.choice([11, 12, 13, 14]), 0.2 * np.random.normal(1, 0.2)] for i in
                 range(30)]
            outpoints = []

        elif part == FacePart.EAR:
            pass

        elif part == FacePart.NOSE_WITH_MOUTH:
            midpoints = \
                [[random.choice(rightdowneye), random.choice(nosecolumn), 0.5 * np.random.normal(1, 0.2)] for i in
                range(30)] + \
                [[random.choice(leftdowneye), random.choice(nosecolumn), 0.5 * np.random.normal(1, 0.2)] for i in
                range(30)] + \
                [[random.choice(nosefloor), random.choice(rightdowneye), 0.2 * np.random.normal(1, 0.5)] for i in
                range(30)] + \
                [[random.choice(nosefloor), random.choice(leftdowneye), 0.2 * np.random.normal(1, 0.5)] for i in
                range(30)] + \
                [[random.choice(nosefloor), random.choice(uplips), 0.5 * np.random.normal(1, 0.2)] for i in
                range(30)]

            midpoints += \
                [[random.choice(uplips), random.choice(nosefloor), 0.5 * np.random.normal(1, 0.2)] for i in
                range(30)] + \
                [[random.choice(leftdownlips), random.choice(leftjaw), 0.2 * np.random.normal(1, 0.2)] for i in
                 range(30)] + \
                [[random.choice(rightdownlips), random.choice(rightjaw), 0.2 * np.random.normal(1, 0.2)] for i in
                 range(30)] + \
                [[random.choice([48]), random.choice([2, 3, 4, 5]), 0.2 * np.random.normal(1, 0.2)] for i in
                 range(30)] + \
                [[random.choice([54]), random.choice([11, 12, 13, 14]), 0.2 * np.random.normal(1, 0.2)] for i in
                 range(30)]

            outpoints = \
                [[random.choice(nosefloor), random.choice(nosefloor), 0.1 * np.random.normal(1,0.3)] for i in
                range(20)]

        elif part == FacePart.NOSE_WITH_EYE:
            midpoints = \
                [[random.choice([0, 1, 2]), random.choice([17, 18, 36]), 0.5 * np.random.normal(1, 0.1)] for i in
                 range(30)] + \
                [[random.choice([16, 15, 14]), random.choice([26, 45, 25]), 0.5 * np.random.normal(1, 0.1)] for i in
                 range(30)]

            midpoints += \
                [[random.choice(rightdowneye), random.choice(nosecolumn), 0.5 * np.random.normal(1, 0.2)] for i in
                range(30)] + \
                [[random.choice(leftdowneye), random.choice(nosecolumn), 0.5 * np.random.normal(1, 0.2)] for i in
                range(30)] + \
                [[random.choice(nosefloor), random.choice(rightdowneye), 0.2 * np.random.normal(1, 0.5)] for i in
                range(30)] + \
                [[random.choice(nosefloor), random.choice(leftdowneye), 0.2 * np.random.normal(1, 0.5)] for i in
                range(30)] + \
                [[random.choice(nosefloor), random.choice(uplips), 0.5 * np.random.normal(1, 0.2)] for i in
                range(30)]

            outpoints = \
                [[random.choice(nosefloor), random.choice(nosefloor), 0.1 * np.random.normal(1,0.3)] for i in
                range(20)]

            outpoints += \
                [[random.choice(rightupeye), random.choice(rightdowneye), 0.5 * np.random.normal(1, 0.1)] for i in
                 range(20)] + \
                [[random.choice(rightdowneye), random.choice(rightupeye), 0.8 * np.random.normal(1, 0.3)] for i in
                 range(20)] + \
                [[random.choice(leftupeye), random.choice(leftdowneye), 0.5 * np.random.normal(1, 0.1)] for i in
                 range(20)] + \
                [[random.choice(leftdowneye), random.choice(leftupeye), 0.8 * np.random.normal(1, 0.3)] for i in
                 range(20)]


        return midpoints, outpoints


def face_swap(src, dst, save_dir, syn_name, back_name):
    try:
        # 얼굴 읽고 landmark 계산
        face1 = Face(src, None)
        face2 = Face(dst, None)
        # 각 얼굴의 오르쪽, 왼쪽 눈, 코 사이 거리 측정후 너무 다른 영상이면 pass
        v1 = face1.distance_pts(pairs[2]) / face1.distance_pts(pairs[3])
        v2 = face2.distance_pts(pairs[2]) / face2.distance_pts(pairs[3])
        if np.abs(v1 / v2 - 1) > 0.3:
            return False

        if random.random() < 0.25:
            difficulty = FacePart.Easy
            part = random.choice(
                [FacePart.MOUTH, FacePart.NOSE_WITH_EYE, FacePart.EYE, FacePart.NOSE, FacePart.NOSE_WITH_MOUTH])

        else:
            difficulty = FacePart.Hard
            part = FacePart.FACE

        if random.random() < 0.5:
            affinePerspective = True
        else:
            affinePerspective = False

        out_im, back_im, valid = face_swap_detail(face1, face2, difficulty=difficulty,
                                                  affinePerspective=affinePerspective,
                                                  part=part, useBlending=False,
                                                  opacity_value=0.5 * random.random() + 0.25,
                                                  crop=True, seamNormal=True)

        if valid:
            cv2.imwrite(save_dir + syn_name, out_im, [cv2.IMWRITE_JPEG_QUALITY, 100])
        if len(back_im) > 0:
            cv2.imwrite(save_dir + back_name, back_im, [cv2.IMWRITE_JPEG_QUALITY, 100])
    except:
        return False


def face_swap_detail(face1, face2, difficulty=FacePart.Easy, affinePerspective=False, part=FacePart.NOSE_WITH_EYE,
                     useBlending=True, opacity_value=0.5, crop=False,
                     seamNormal=True):
    # print("Hi")

    midpoints, outpoints = FacePart.partPoints(part)
    if affinePerspective:
        # 눈 끝 두 점과 입술 세점 측정
        eye1 = [face1.landmarks[pairs[0][0]], face1.landmarks[pairs[0][1]], face1.landmarks[57]]
        eye2 = [face2.landmarks[pairs[0][0]], face2.landmarks[pairs[0][1]], face2.landmarks[57]]

    else:
        # 눈 끝 두 점과 직각 이등변 이루는점 측정
        eye1 = [face1.landmarks[pairs[0][0]], face1.landmarks[pairs[0][1]], [0, 0]]
        eye1[2] = rotatePoint(eye1[0], eye1[1], np.pi / 2)
        eye2 = [face2.landmarks[pairs[0][0]], face2.landmarks[pairs[0][1]], [0, 0]]
        eye2[2] = rotatePoint(eye2[0], eye2[1], np.pi / 2)

    eye1 = np.array([(x, y) for x, y in eye1], np.float32)
    eye2 = np.array([(x, y) for x, y in eye2], np.float32)

    # 각 이미지의 세 점을 기준으로 한 affinetransform matrix 계산
    M = cv2.getAffineTransform(eye1, eye2)
    x, y, _ = face2.image.shape

    # 얼굴 transformation 하면서 landmark도 같이 transform
    face1.warpAffine(M, (y, x))

    # 얼굴 더 큰 사람 측정
    len1 = face1.distance_pts(pairs[4])
    len2 = face2.distance_pts(pairs[4])

    # 더 작은 얼굴에서 자를 때 사용할 얼굴 안쪽 점과 눈썹 위의 점 계산
    if len1 < len2:
        low_p = [face1.in_pt_amount(l1, l2, amount) for l1, l2, amount in midpoints]
        up_p = [face1.out_pt_amount(l1, l2, amount) for l1, l2, amount in outpoints]
    else:
        low_p = [face2.in_pt_amount(l1, l2, amount) for l1, l2, amount in midpoints]
        up_p = [face2.out_pt_amount(l1, l2, amount) for l1, l2, amount in outpoints]
    surrounding = np.array([(x, y) for x, y in low_p + up_p], np.float32)

    # point를 감싸는 Polygon 계산
    surrounding = cv2.convexHull(surrounding)
    xx, yy, zz = np.shape(surrounding)
    surrounding = np.reshape(surrounding, [xx, zz])

    # Polygon의 마스크
    surrounding = np.array([(x, y) for x, y in surrounding], np.float32)
    mask = np.zeros((x, y, 3), dtype=np.float32)
    cv2.fillConvexPoly(mask, np.array(surrounding, np.int32), (1, 1, 1))

    if difficulty == FacePart.Hard:
        # Mask 기준으로 rough한 얼굴 합성
        if useBlending:
            # 다양한 blending으로 두 layer 합성 (mask 인근만)
            blending_type = random.randrange(1, 15)
            # 가능한 리스트 - soft_light(1), lighten_only(2), dodge(3), addition(4), darken_only(5), multiply(6),
            # hard_light(7), difference(8), subtract(9), grain_extract(10), grain_merge(11), divide(12), overlay(13), normal(14)
            raw = blending(face1.image, face2.image, mask, blending_type, opacity_value)
        else:
            raw = face2.image * (1.0 - mask) + face1.image * mask

        # 합성 부의의 center 계산
        r = cv2.boundingRect(surrounding)
        center = ((r[0] + int(r[2] / 2), r[1] + int(r[3] / 2)))

        if seamNormal:
            output_im = cv2.seamlessClone(src=np.uint8(raw), dst=face2.image, mask=(mask * 255).astype(np.uint8),
                                          p=center, flags=cv2.NORMAL_CLONE)
        else:
            output_im = cv2.seamlessClone(src=np.uint8(raw), dst=face2.image, mask=(mask * 255).astype(np.uint8),
                                          p=center,
                                          flags=cv2.MIXED_CLONE)

    else:
        kernel_size = random.choice(list(range(31, 50, 2)))
        mask = cv2.GaussianBlur(mask, (kernel_size, kernel_size), 0)
        output_im = face2.image * (1.0 - mask) + face1.image * mask

    background = None
    if crop:
        back_cropped = face2.facecrop(128)
        if back_cropped:
            background = np.empty_like(face2.cropped_face)
            background[:] = face2.cropped_face

        face2.image = np.ndarray.astype(output_im, np.uint8)
        cropped = face2.facecrop(128)
        if cropped:
            output_im = face2.cropped_face
        else:
            return None, None, False

    return output_im, background, True


def main():
    save_dir = './../../../data/'
    face_dirs= './../../../data/real/*'

    folders = glob.glob(face_dirs)
    random.shuffle(folders)
    folders_ = glob.glob(face_dirs)

    random.shuffle(folders_)
    for i in range(100):
        folder = random.choice(folders)
        folder_ = random.choice(folders_)
        print('make {}_{} '.format(folder, folder_))

        files = glob.glob('{}/*.jpg'.format(folder))
        files_ = glob.glob('{}/*.jpg'.format(folder_))

        random.shuffle(files_)
        random.shuffle(files)

        try:
            os.mkdir(save_dir + 'syn_cropped/{}'.format(i))
            os.mkdir(save_dir + 'syn_background/{}'.format(i))
        except:
            pass

        # for j in range(1000):
        #    face_swap(files[j], files_[j], './../../../data/syn_cropped/{}/{}.jpg'.format(i, j))
        Parallel(n_jobs=6)(delayed(face_swap)(files[j], files_[j], save_dir, 'syn_cropped/{}/{}.jpg'.format(i, j),
                                              'syn_background/{}/{}.jpg'.format(i, j)) for j in range(1000))


if __name__ == '__main__':
    # face_swap('src.jpg','dst.jpg', './','out.jpg', 'back.jpg')

    main()














