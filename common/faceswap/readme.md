USAGE
========================

    from faceswap.Face import Face

    img_size = 128
    if image == None:
        face = Face('src.jpg', None)
    else:
        face = Face(None, image)
    valid = face.facecrop(img_size)
    if valid:
	    cropped_face = face.cropped_face


#requirements : python3, opencv, dlib, blend_mode(https://github.com/flrs/blend_modes)